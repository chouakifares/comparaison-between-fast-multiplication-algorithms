# comparaison-between-fast-multiplication-algorithms
This projects aims to implement fast multiplication algorithms (namely karatsuba , toom-cook 3 and FFT) on polynomials. 
 
## Setup 

You need to be able to compile c code in order to test the efficiency of the algos. 
We used the [gcc compiler](https://gcc.gnu.org/install/download.html) but feel free to use any compiler you feel comfortable with.

